package com.lotso.web.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * ClassName: WebMvcConfig
 * Description: SpringMVc 配置
 * Spring5.X 以后 WebMvcConfigurerAdapter 过期，直接实现 WebMvcConfigurer 接口
 * Date: 2018/9/1 7:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 配置简单的视图访问
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/").setViewName("index");
    }
}
