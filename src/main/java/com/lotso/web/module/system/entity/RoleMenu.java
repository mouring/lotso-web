package com.lotso.web.module.system.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractBaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: RoleAuthorities
 * Description: 角色权限关联表
 * Date: 2018/9/2 7:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@TableName("t_sys_role_menu")
public class RoleMenu extends AbstractBaseEntity {

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 权限id
     */
    private String authorityId;

}