package com.lotso.web.module.batch.controller;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.module.base.AbstractController;
import com.lotso.web.module.batch.entity.BatchSchedule;
import com.lotso.web.module.batch.entity.BatchTask;
import com.lotso.web.module.batch.service.impl.BatchScheduleServiceImpl;
import com.lotso.web.module.batch.service.impl.BatchTaskServiceImpl;
import com.lotso.web.module.system.entity.Dict;
import com.lotso.web.module.system.service.impl.DictServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * ClassName: BatchScheduleController
 * Description: 任务计划控制
 * Date: 2018/9/10 15:20 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("batch/schedule")
public class BatchScheduleController extends AbstractController {

    @Autowired
    private BatchScheduleServiceImpl scheduleService;
    @Autowired
    private BatchTaskServiceImpl taskService;
    @Autowired
    private DictServiceImpl dictService;

    /**
     * 进入查询页面
     *
     * @return
     */
    @PreAuthorize("hasAuthority('schedule:view')")
    @GetMapping
    public String list() {
        return "module/batch/schedule";
    }

    /**
     * 查询列表
     */
    @PreAuthorize("hasAuthority('schedule:view')")
    @ResponseBody
    @GetMapping("/list")
    public Result<BatchSchedule> list(ReqParam reqParam) {
        return scheduleService.list(reqParam);
    }

    /**
     * 进入添加或者修改页面
     *
     * @param
     * @return
     */
    @PreAuthorize("hasAuthority('schedule:add')")
    @RequestMapping("/editForm")
    public String editForm(Model model) {
        List<BatchTask> tasks = taskService.list();
        model.addAttribute("tasks", tasks);

        List<BatchSchedule> schedules = scheduleService.list();
        model.addAttribute("schedules", schedules);

        // 字典表数据
        List<Dict> cronTypes = dictService.listByType("CronType");
        model.addAttribute("cronTypes", cronTypes);

        return "module/batch/schedule_form";
    }

    /**
     * 添加计划
     **/
    @PreAuthorize("hasAuthority('schedule:add')")
    @ResponseBody
    @RequestMapping("/add")
    public Result add(BatchSchedule batchSchedule) {
        if (scheduleService.add(batchSchedule)) {
            return Result.ok("添加成功");
        } else {
            return Result.error("添加失败");
        }
    }

    /**
     * 修改计划
     **/
    @PreAuthorize("hasAuthority('schedule:edit')")
    @ResponseBody
    @RequestMapping("/update")
    public Result update(BatchSchedule batchSchedule) {
        if (scheduleService.update(batchSchedule)) {
            return Result.ok("修改成功");
        } else {
            return Result.error("修改失败");
        }
    }

    /**
     * 删除计划
     **/
    @PreAuthorize("hasAuthority('schedule:delete')")
    @ResponseBody
    @RequestMapping("/delete")
    public Result delete(String scheduleId) {
        if (scheduleService.delete(scheduleId)) {
            return Result.ok("删除成功");
        } else {
            return Result.error("删除失败");
        }
    }

    /**
     * 启动任务计划（加入执行队列）
     *
     * @param scheduleId
     * @return
     */
    @PreAuthorize("hasAuthority('schedule:run')")
    @ResponseBody
    @RequestMapping("/run")
    public Result run(String scheduleId) {
        if (scheduleService.run(scheduleId)) {
            return Result.ok("任务计划成功加入启动队列");
        } else {
            return Result.error("任务计划启动失败，请联系管理员！");
        }
    }

    /**
     * 暂停计划
     *
     * @param scheduleId
     * @return
     */
    @PreAuthorize("hasAuthority('schedule:pause')")
    @ResponseBody
    @RequestMapping("/pause")
    public Result pause(String scheduleId) {
        if (scheduleService.pause(scheduleId)) {
            return Result.ok("任务计划暂停成功");
        } else {
            return Result.error("任务计划暂停失败，请联系管理员！");
        }
    }

    /**
     * 从暂停中恢复
     *
     * @param scheduleId
     * @return
     */
    @PreAuthorize("hasAuthority('schedule:resume')")
    @ResponseBody
    @RequestMapping("/resume")
    public Result resume(String scheduleId) {
        if (scheduleService.resume(scheduleId)) {
            return Result.ok("任务计划恢复运行成功");
        } else {
            return Result.error("任务计划恢复运行失败，请联系管理员！");
        }
    }
}
