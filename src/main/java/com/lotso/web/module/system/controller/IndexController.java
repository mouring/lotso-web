package com.lotso.web.module.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: IndexController
 * Description:
 * Date: 2018/11/7 10:32 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("/index")
public class IndexController {


    @RequestMapping
    public String index() {
        return "index";
    }


    /**
     * 欢迎页面
     *
     * @return
     */
    @RequestMapping("/home")
    public String home() {
        return "index/home";
    }

    /**
     * 控制台
     *
     * @return
     */
    @RequestMapping("/console")
    public String console() {
        return "index/console";
    }

    /**
     * 显示便签
     *
     * @return
     */
    @RequestMapping("/note")
    public String note() {
        return "index/note";
    }

    /**
     * 消息弹窗
     */
    @RequestMapping("/message")
    public String message() {
        return "index/message";
    }

    /**
     * 显示设置
     *
     * @return
     */
    @RequestMapping("/setting")
    public String setting() {
        return "index/setting";
    }


}
