package com.lotso.web.module.system.service;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.common.exception.ParameterException;
import com.lotso.web.module.system.entity.User;

/**
 * ClassName: UserService
 * Description: 用户业务
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IUserService {

    /**
     * 通过账号查询用户信息
     *
     * @param username
     * @return
     */
    User getByUsername(String username);

    /**
     * 获取个人信息
     *
     * @param username
     * @return
     */
    User getDetailByUsername(String username);

    /**
     * 根据条件查询用户列表
     *
     * @param reqParam 请求参数对象
     * @return
     */
    Result<User> list(ReqParam reqParam);

    /**
     * 根据用户ID
     *
     * @param userId
     * @return
     */
    User getById(String userId);

    /**
     * 新增用户，默认密码
     *
     * @param user 用户信息
     * @return boolean
     * @throws BusinessException
     */
    boolean add(User user) throws BusinessException;

    /**
     * 修改用户
     *
     * @param user 用户信息
     * @return boolean
     */
    boolean update(User user);

    /**
     * 修改用户状态
     *
     * @param userId
     * @param state
     * @return
     * @throws ParameterException
     */
    boolean updateState(String userId, int state) throws ParameterException;

    /**
     * 修改密码
     *
     * @param userId
     * @param newPsw
     * @return
     */
    boolean updatePwd(String userId, String newPsw);

    /**
     * 删除用户
     *
     * @param userId
     * @return
     */
    boolean delete(String userId);

}
