package com.lotso.web.module.system.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.system.entity.RoleMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:
 * Description:
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IRoleMenuMapper extends IBaseMapper<RoleMenu> {

    /**
     * 批量更新角色分配的权限列表
     *
     * @param roleId
     * @param menuIds
     * @return
     */
    int insertRoleAuths(@Param("roleId") String roleId, @Param("menuIds") List<String> menuIds);

    /**
     * 批量更新角色分配的权限列表（Oracle）
     *
     * @param roleId
     * @param menuIds
     * @return
     */
    int insertRoleAuthsForOracle(@Param("roleId") String roleId, @Param("menuIds") List<String> menuIds);

}
