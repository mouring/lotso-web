/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50710
Source Host           : localhost:3306
Source Database       : lotso_web

Target Server Type    : MYSQL
Target Server Version : 50710
File Encoding         : 65001

Date: 2018-10-29 13:54:48
*/

CREATE DATABASE if NOT EXISTS lotso_web;
USE lotso_web;

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_batch_schedule
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_schedule`;
CREATE TABLE `t_batch_schedule` (
  ` ID ` VARCHAR (24
) NOT NULL,
  `code` VARCHAR (32
) NOT NULL COMMENT '计划编码',
  ` NAME ` VARCHAR (64
) NOT NULL COMMENT '计划名称',
  `status` INT (1
) NOT NULL DEFAULT '0' COMMENT '计划状态',
  `cronType` INT (1
) DEFAULT '1' COMMENT '表达式类型，默认为 1-执行一次',
  `cronExpression` VARCHAR (64
) DEFAULT NULL COMMENT '执行表达式',
  `interfaceName` VARCHAR (255
) DEFAULT NULL,
  `serverIp` VARCHAR (64
) DEFAULT NULL COMMENT '运行服务器Ip',
  `description` VARCHAR (255
) DEFAULT NULL COMMENT '描述',
  `taskCode` VARCHAR (32
) NOT NULL,
  `startDate` datetime DEFAULT NULL COMMENT '最近开始时间',
  `endDate` datetime DEFAULT NULL COMMENT '最近结束时间',
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (` ID `
),
UNIQUE KEY `t_batch_schedule_code_uindex` (`code`
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8 COMMENT ='批处理任务计划表';

-- ----------------------------
-- Records of t_batch_schedule
-- ----------------------------
INSERT INTO `t_batch_schedule` VALUES ('1', 'BS1040795600922087426', '计划测试-01', '3', '1', '', 'com.lotso.web.module.batch.job.impl.SimpleJob', '127.0.0.1', '基本任务-计划测试', 'BT1039705122717872130', '2018-10-22 10:56:12', '2018-10-22 10:56:12', '2018-09-15 10:53:02', '2018-10-22 10:56:12');
INSERT INTO `t_batch_schedule` VALUES ('2', 'BS1052796513152958466', '计划测试-02', '3', '1', '', 'com.lotso.web.module.batch.job.impl.SimpleJob', '', '基本任务-计划测试2', 'BT1039705122717872130', '2018-10-22 10:56:17', '2018-10-22 10:56:17', '2018-10-18 13:40:30', '2018-10-22 10:56:17');

-- ----------------------------
-- Table structure for t_batch_schedule_dependency
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_schedule_dependency`;
CREATE TABLE `t_batch_schedule_dependency` (
  `scheduleId` VARCHAR (24
) DEFAULT NULL COMMENT '计划ID',
  `dependencyId` VARCHAR (24
) DEFAULT NULL COMMENT '依赖的前置任务ID',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET =utf8 COMMENT ='前置计划关系表';

-- ----------------------------
-- Records of t_batch_schedule_dependency
-- ----------------------------
INSERT INTO `t_batch_schedule_dependency` VALUES ('2', '1', '2018-10-18 15:36:03');

-- ----------------------------
-- Table structure for t_batch_schedule_param
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_schedule_param`;
CREATE TABLE `t_batch_schedule_param` (
  `scheduleId` VARCHAR (24
) DEFAULT NULL COMMENT '任务计划Id',
  `scheduleCode` VARCHAR (32
) DEFAULT NULL COMMENT '任务计划Code',
  `paramName` VARCHAR (32
) DEFAULT NULL COMMENT '参数名',
  `paramValue` VARCHAR (32
) DEFAULT NULL COMMENT '参数值',
  `tempId` VARCHAR (64
) DEFAULT NULL COMMENT '参数描述'
) ENGINE=InnoDB DEFAULT CHARSET =utf8 COMMENT ='任务计划参数';

-- ----------------------------
-- Records of t_batch_schedule_param
-- ----------------------------
INSERT INTO `t_batch_schedule_param` VALUES ('2', 'BS1052796513152958466', 'name', 'sam', '1539848015027');
INSERT INTO `t_batch_schedule_param` VALUES ('1', 'BS1040795600922087426', 'name', 'sam', '1537096814399');
INSERT INTO `t_batch_schedule_param` VALUES ('1', 'BS1040795600922087426', 'address', 'suzhou', '1537096837591');

-- ----------------------------
-- Table structure for t_batch_task
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_task`;
CREATE TABLE `t_batch_task` (
  ` ID ` VARCHAR (24
) NOT NULL,
  `code` VARCHAR (32
) NOT NULL COMMENT '任务编码',
  ` NAME ` VARCHAR (64
) NOT NULL COMMENT '任务名称',
  `description` VARCHAR (128
) DEFAULT NULL COMMENT '任务描述',
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (` ID `
),
UNIQUE KEY `t_batch_job_group_code_uindex` (`code`
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8 COMMENT ='批量任务组';

-- ----------------------------
-- Records of t_batch_task
-- ----------------------------
INSERT INTO `t_batch_task` VALUES ('1', 'BT1039705122717872130', '基础任务', '基础任务', '2018-09-12 10:39:59', '2018-09-12 11:32:42');

-- ----------------------------
-- Table structure for t_org_area
-- ----------------------------
DROP TABLE IF EXISTS `t_org_area`;
CREATE TABLE `t_org_area` (
  ` ID ` VARCHAR (24
) NOT NULL,
  `code` VARCHAR (16
) DEFAULT NULL COMMENT '区域编码',
  ` NAME ` VARCHAR (128
) DEFAULT NULL COMMENT '区域名称',
  `parentId` VARCHAR (24
) DEFAULT '-1',
  ` TYPE ` INT (11
) NOT NULL COMMENT '区域类型',
  ` SORT ` INT (11
) DEFAULT NULL,
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (` ID `
),
UNIQUE KEY `t_sys_area_code_uindex` (`code`
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8 COMMENT ='组织区域表';

-- ----------------------------
-- Records of t_org_area
-- ----------------------------
INSERT INTO `t_org_area` VALUES ('1', '86', '中国', '-1', '1', '0', '2018-09-27 21:50:38', '2018-09-27 21:50:38');
INSERT INTO `t_org_area` VALUES ('2', '8601', '江苏省', '1', '2', '10', '2018-09-27 21:54:05', '2018-09-27 21:54:05');
INSERT INTO `t_org_area` VALUES ('3', '860101', '苏州市', '2', '3', '20', '2018-09-27 22:22:35', '2018-09-27 22:22:35');
INSERT INTO `t_org_area` VALUES ('4', '86010101', '高新区', '3', '4', '21', '2018-09-27 22:28:48', '2018-09-27 22:28:48');

-- ----------------------------
-- Table structure for t_org_office
-- ----------------------------
DROP TABLE IF EXISTS `t_org_office`;
CREATE TABLE `t_org_office` (
  ` ID ` VARCHAR (24
) NOT NULL,
  `areaId` VARCHAR (24
) DEFAULT NULL COMMENT '归属区域',
  `code` VARCHAR (32
) NOT NULL COMMENT '机构编码',
  ` NAME ` VARCHAR (32
) DEFAULT NULL COMMENT '机构名称',
  ` TYPE ` INT (11
) DEFAULT NULL COMMENT '机构类型',
  `grade` INT (11
) DEFAULT NULL COMMENT '机构等级',
  `address` VARCHAR (128
) DEFAULT NULL COMMENT '联系地址',
  `zipCode` VARCHAR (24
) DEFAULT NULL COMMENT '邮政编码',
  `phone` VARCHAR (24
) DEFAULT NULL COMMENT '电话',
  `fax` VARCHAR (24
) DEFAULT NULL COMMENT '传真',
  ` MASTER ` VARCHAR (24
) DEFAULT NULL COMMENT '负责人',
  `primaryUser` INT (24
) DEFAULT NULL COMMENT '主负责人',
  `deputyUser` INT (24
) DEFAULT NULL COMMENT '副负责人',
  `description` VARCHAR (128
) DEFAULT NULL COMMENT '备注',
  `parentId` VARCHAR (24
) DEFAULT '-1' COMMENT '上级机构',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime DEFAULT CURRENT_TIMESTAMP,
  ` SORT ` INT (8
) DEFAULT NULL COMMENT '排序',
PRIMARY KEY (` ID `
),
UNIQUE KEY `t_sys_office_code_uindex` (`code`
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8 COMMENT ='组织机构表';

-- ----------------------------
-- Records of t_org_office
-- ----------------------------
INSERT INTO `t_org_office` VALUES ('1', '1', '86', '86-东吴人寿', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-1', '2018-09-28 17:19:07', '2018-09-28 17:19:07', '0');
INSERT INTO `t_org_office` VALUES ('2', '3', '8601', '8601-苏州分公司', '1', '1', '', '', '', '', NULL, NULL, NULL, NULL, '1', '2018-09-28 17:35:25', '2018-09-28 17:35:25', '10');
INSERT INTO `t_org_office` VALUES ('3', '3', '860101', '860101-苏州分公司', '1', '2', '', '', '', '', NULL, NULL, NULL, NULL, '2', '2018-09-29 11:00:09', '2018-09-29 11:00:09', '11');
INSERT INTO `t_org_office` VALUES ('4', '3', '86010106', '信息技术部', '2', '4', '', '', '', '', NULL, NULL, NULL, NULL, '3', '2018-10-11 15:56:10', '2018-10-11 15:56:10', NULL);

-- ----------------------------
-- Table structure for t_sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict`;
CREATE TABLE `t_sys_dict` (
  ` ID ` VARCHAR (24
) NOT NULL COMMENT '编号',
  ` VALUE ` VARCHAR (100
) NOT NULL COMMENT '数据值',
  `label` VARCHAR (100
) NOT NULL COMMENT '标签名',
  ` TYPE ` VARCHAR (100
) NOT NULL COMMENT '类型',
  `description` VARCHAR (100
) NOT NULL COMMENT '描述',
  ` SORT ` INT (8
) DEFAULT NULL COMMENT '排序（升序）',
  `parentId` VARCHAR (24
) DEFAULT '0' COMMENT '父级编号',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (` ID `
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE = 1 ROW_FORMAT = DYNAMIC COMMENT = '字典表';

-- ----------------------------
-- Records of t_sys_dict
-- ----------------------------
INSERT INTO `t_sys_dict` VALUES ('1', '0', '未启用', 'ScheduleStatus', '计划状态', '10', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('10', '2', '每分钟一次', 'CronType', '执行表达式类型', '20', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('11', '3', '每小时一次', 'CronType', '执行表达式类型', '30', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('12', '4', '每天一次', 'CronType', '执行表达式类型', '40', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('13', '5', '每月一次', 'CronType', '执行表达式类型', '50', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('14', '6', '每周一次', 'CronType', '执行表达式类型', '60', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('15', '0', '其他', 'CronType', '执行表达式类型', '70', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('2', '1', '正常启动中', 'ScheduleStatus', '计划状态', '20', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('3', '2', '暂停', 'ScheduleStatus', '计划状态', '30', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('4', '3', '运行完成', 'ScheduleStatus', '计划状态', '40', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('5', '4', '异常失败', 'ScheduleStatus', '计划状态', '50', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('6', '5', '运行阻塞', 'ScheduleStatus', '计划状态', '60', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('7', '6', '启用，未启动', 'ScheduleStatus', '计划状态', '70', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('8', '7', '运行中', 'ScheduleStatus', '计划状态', '80', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');
INSERT INTO `t_sys_dict` VALUES ('9', '1', '执行一次', 'CronType', '执行表达式类型', '10', '0', '2018-07-21 10:03:40', '2018-07-21 10:03:40');

-- ----------------------------
-- Table structure for t_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_log`;
CREATE TABLE `t_sys_log` (
  ` ID ` VARCHAR (24
) NOT NULL COMMENT '主键',
  `username` VARCHAR (20
) NOT NULL COMMENT '用户账号',
  `url` VARCHAR (40
) DEFAULT NULL COMMENT '访问地址',
  `action` VARCHAR (10
) DEFAULT NULL COMMENT '操作动作',
  `args` VARCHAR (1024
) DEFAULT NULL COMMENT '具体参数',
  ` EXCEPTION ` VARCHAR (2048
) DEFAULT NULL COMMENT '异常信息',
  `osName` VARCHAR (24
) DEFAULT NULL COMMENT '操作系统',
  `device` VARCHAR (24
) DEFAULT NULL COMMENT '设备型号',
  `browserType` VARCHAR (40
) DEFAULT NULL COMMENT '浏览器类型',
  `ip` VARCHAR (40
) DEFAULT NULL COMMENT 'ip地址',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
PRIMARY KEY (` ID `
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE = 1 ROW_FORMAT = DYNAMIC COMMENT = '系统日志表';

-- ----------------------------
-- Records of t_sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_menu`;
CREATE TABLE `t_sys_menu` (
  ` ID ` VARCHAR (24
) NOT NULL COMMENT '权限id',
  `menuName` VARCHAR (20
) NOT NULL COMMENT '菜单名称',
  `authority` VARCHAR (40
) DEFAULT NULL COMMENT '授权标识',
  `menuUrl` VARCHAR (40
) DEFAULT NULL COMMENT '菜单url',
  `parentId` VARCHAR (24
) NOT NULL DEFAULT '-1' COMMENT '父id',
  `isMenu` INT (1
) NOT NULL DEFAULT '0' COMMENT '0菜单，1按钮',
  ` SORT ` INT (5
) NOT NULL DEFAULT '0' COMMENT '排序号',
  `menuIcon` VARCHAR (20
) DEFAULT NULL COMMENT '菜单图标',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
PRIMARY KEY (` ID `
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE = 1 ROW_FORMAT = DYNAMIC COMMENT = '菜单权限表';

-- ----------------------------
-- Records of t_sys_menu
-- ----------------------------
INSERT INTO `t_sys_menu` VALUES ('1', '系统管理', NULL, NULL, '-1', '0', '1', 'layui-icon-set', '2018-06-29 11:05:41', '2018-07-13 09:13:42');
INSERT INTO `t_sys_menu` VALUES ('10', '修改角色', 'role:edit', '', '7', '1', '35', '', '2018-06-29 11:05:41', '2018-07-13 09:13:42');
INSERT INTO `t_sys_menu` VALUES ('55', '统计监控', 'charts:view', 'monitor/charts', '23', '0', '150', NULL, '2018-09-27 10:53:04', '2018-09-27 10:56:10');
INSERT INTO `t_sys_menu` VALUES ('41', '区域管理', NULL, 'organization/area', '40', '0', '80', NULL, '2018-09-27 18:54:14', '2018-09-28 16:43:06');
INSERT INTO `t_sys_menu` VALUES ('46', '管理机构', NULL, 'organization/office', '40', '0', '90', NULL, '2018-09-28 13:30:34', '2018-09-28 16:43:12');
INSERT INTO `t_sys_menu` VALUES ('42', '查询区域', 'area:view', NULL, '41', '1', '81', NULL, '2018-09-28 13:59:17', '2018-09-28 14:12:19');
INSERT INTO `t_sys_menu` VALUES ('43', '新增区域', 'area:addBatch', NULL, '41', '1', '83', NULL, '2018-09-28 14:00:47', '2018-09-28 14:12:27');
INSERT INTO `t_sys_menu` VALUES ('44', '修改区域', 'area:edit', NULL, '41', '1', '85', NULL, '2018-09-28 14:01:38', '2018-09-28 14:12:36');
INSERT INTO `t_sys_menu` VALUES ('45', '删除区域', 'area:delete', NULL, '41', '1', '87', NULL, '2018-09-28 14:02:09', '2018-09-28 14:12:43');
INSERT INTO `t_sys_menu` VALUES ('47', '查询机构', 'office:view', NULL, '46', '1', '91', NULL, '2018-09-28 14:13:54', '2018-09-28 14:13:54');
INSERT INTO `t_sys_menu` VALUES ('48', '新增机构', 'office:addBatch', NULL, '46', '1', '93', NULL, '2018-09-28 14:30:16', '2018-09-28 14:30:16');
INSERT INTO `t_sys_menu` VALUES ('49', '修改机构', 'office:edit', NULL, '46', '1', '95', NULL, '2018-09-28 14:30:59', '2018-09-28 14:30:59');
INSERT INTO `t_sys_menu` VALUES ('50', '删除机构', 'office:delete', NULL, '46', '1', '97', NULL, '2018-09-28 14:31:25', '2018-09-28 14:31:25');
INSERT INTO `t_sys_menu` VALUES ('40', '机构管理', '', '', '-1', '0', '2', 'layui-icon-read', '2018-09-28 15:46:49', '2018-09-30 10:14:51');
INSERT INTO `t_sys_menu` VALUES ('51', '主页', '', '', '-1', '0', '0', 'layui-icon-home', '2018-10-09 11:40:28', '2018-10-11 09:19:01');
INSERT INTO `t_sys_menu` VALUES ('52', '首页面', '', 'index/home', '51', '0', '1', '', '2018-10-09 11:40:58', '2018-10-09 11:47:45');
INSERT INTO `t_sys_menu` VALUES ('53', '开发文档', '', 'index/console', '51', '0', '2', '', '2018-10-09 11:41:26', '2018-10-09 11:42:08');
INSERT INTO `t_sys_menu` VALUES ('54', '个人中心', '', 'system/user/detail', '1', '0', '5', '', '2018-10-18 10:25:14', '2018-10-18 10:25:14');
INSERT INTO `t_sys_menu` VALUES ('11', '删除角色', 'role:delete', '', '7', '1', '37', '', '2018-06-29 11:05:41', '2018-07-13 09:13:42');
INSERT INTO `t_sys_menu` VALUES ('12', '角色权限管理', 'role:auth', '', '7', '1', '39', '', '2018-06-29 11:05:41', '2018-07-13 15:27:18');
INSERT INTO `t_sys_menu` VALUES ('13', '菜单管理', '', 'system/menu', '1', '0', '50', '', '2018-06-29 11:05:41', '2018-09-30 10:15:46');
INSERT INTO `t_sys_menu` VALUES ('14', '查询菜单', 'menu:view', '', '13', '1', '51', '', '2018-09-03 13:25:04', '2018-09-03 13:25:04');
INSERT INTO `t_sys_menu` VALUES ('15', '添加菜单', 'menu:addBatch', '', '13', '1', '53', '', '2018-09-03 13:25:21', '2018-09-03 13:25:21');
INSERT INTO `t_sys_menu` VALUES ('16', '修改菜单', 'menu:edit', '', '13', '1', '55', '', '2018-09-03 13:25:37', '2018-09-03 13:25:37');
INSERT INTO `t_sys_menu` VALUES ('17', '删除菜单', 'menu:delete', '', '13', '1', '57', '', '2018-09-03 13:25:49', '2018-09-03 13:25:49');
INSERT INTO `t_sys_menu` VALUES ('18', '字典管理', NULL, 'system/dict', '1', '0', '70', '', '2018-09-13 19:24:20', '2018-09-15 11:30:49');
INSERT INTO `t_sys_menu` VALUES ('19', '字典查询', 'dict:view', '', '18', '1', '71', NULL, '2018-09-13 19:26:02', '2018-09-13 20:01:52');
INSERT INTO `t_sys_menu` VALUES ('2', '用户管理', '', 'system/user', '1', '0', '10', '', '2018-06-29 11:05:41', '2018-09-30 10:15:37');
INSERT INTO `t_sys_menu` VALUES ('20', '字典添加', 'dict:addBatch', NULL, '18', '1', '73', NULL, '2018-09-13 19:43:44', '2018-09-13 20:02:04');
INSERT INTO `t_sys_menu` VALUES ('21', '字典修改', 'dict:edit', NULL, '18', '1', '75', NULL, '2018-09-13 19:44:20', '2018-09-13 20:02:14');
INSERT INTO `t_sys_menu` VALUES ('22', '字典删除', 'dict:delete', NULL, '18', '1', '77', NULL, '2018-09-13 19:44:47', '2018-09-13 20:02:22');
INSERT INTO `t_sys_menu` VALUES ('23', '系统监控', '', '', '-1', '0', '100', 'layui-icon-senior', '2018-09-03 13:47:49', '2018-09-15 11:29:58');
INSERT INTO `t_sys_menu` VALUES ('24', '系统日志', 'log:view', 'monitor/log', '23', '0', '110', NULL, '2018-09-03 13:54:53', '2018-09-03 13:54:53');
INSERT INTO `t_sys_menu` VALUES ('26', '批量任务', NULL, NULL, '-1', '0', '200', 'layui-icon-find-fill', '2018-09-10 15:39:25', '2018-09-15 11:30:10');
INSERT INTO `t_sys_menu` VALUES ('27', '基本任务', NULL, '/batch/task', '26', '0', '210', NULL, '2018-09-10 15:39:51', '2018-09-11 10:25:41');
INSERT INTO `t_sys_menu` VALUES ('28', '任务计划', NULL, 'batch/schedule', '26', '0', '230', NULL, '2018-09-10 15:40:41', '2018-09-11 11:29:22');
INSERT INTO `t_sys_menu` VALUES ('29', '查询任务', 'task:view', NULL, '27', '1', '211', NULL, '2018-09-15 11:44:02', '2018-09-15 11:44:02');
INSERT INTO `t_sys_menu` VALUES ('3', '查询用户', 'user:view', '', '2', '1', '11', '', '2018-07-21 13:54:16', '2018-09-15 11:31:42');
INSERT INTO `t_sys_menu` VALUES ('30', '新增任务', 'task:addBatch', NULL, '27', '1', '213', NULL, '2018-09-15 11:44:02', '2018-09-15 11:44:02');
INSERT INTO `t_sys_menu` VALUES ('31', '修改任务', 'task:edit', NULL, '27', '1', '215', NULL, '2018-09-15 11:44:02', '2018-09-15 11:44:02');
INSERT INTO `t_sys_menu` VALUES ('32', '删除任务', 'task:delete', NULL, '27', '1', '217', NULL, '2018-09-15 11:44:02', '2018-09-15 11:44:02');
INSERT INTO `t_sys_menu` VALUES ('33', '查询计划', 'schedule:view', NULL, '28', '1', '231', NULL, '2018-09-15 11:48:13', '2018-09-15 11:48:13');
INSERT INTO `t_sys_menu` VALUES ('34', '新增计划', 'schedule:addBatch', NULL, '28', '1', '233', NULL, '2018-09-15 11:48:13', '2018-09-15 11:48:13');
INSERT INTO `t_sys_menu` VALUES ('35', '修改计划', 'schedule:edit', NULL, '28', '1', '235', NULL, '2018-09-15 11:48:13', '2018-09-15 11:48:13');
INSERT INTO `t_sys_menu` VALUES ('36', '删除计划', 'schedule:delete', NULL, '28', '1', '237', NULL, '2018-09-15 11:48:13', '2018-09-15 11:48:13');
INSERT INTO `t_sys_menu` VALUES ('37', '启动计划', 'schedule:run', NULL, '28', '1', '239', NULL, '2018-09-15 11:48:13', '2018-09-15 11:48:13');
INSERT INTO `t_sys_menu` VALUES ('38', '暂停计划', 'schedule:pause', NULL, '28', '1', '241', NULL, '2018-09-15 11:48:13', '2018-09-15 11:48:13');
INSERT INTO `t_sys_menu` VALUES ('39', '恢复计划', 'schedule:resume', NULL, '28', '1', '243', NULL, '2018-09-15 11:48:13', '2018-09-15 11:48:13');
INSERT INTO `t_sys_menu` VALUES ('4', '添加用户', 'user:addBatch', NULL, '2', '1', '13', NULL, '2018-06-29 11:05:41', '2018-09-15 11:31:56');
INSERT INTO `t_sys_menu` VALUES ('5', '修改用户', 'user:edit', NULL, '2', '1', '15', NULL, '2018-06-29 11:05:41', '2018-09-15 11:32:04');
INSERT INTO `t_sys_menu` VALUES ('6', '删除用户', 'user:delete', NULL, '2', '1', '17', NULL, '2018-06-29 11:05:41', '2018-09-15 11:32:16');
INSERT INTO `t_sys_menu` VALUES ('7', '角色管理', '', 'system/role', '1', '0', '30', '', '2018-06-29 11:05:41', '2018-09-30 10:15:24');
INSERT INTO `t_sys_menu` VALUES ('8', '查询角色', 'role:view', '', '7', '1', '31', '', '2018-07-21 13:54:59', '2018-09-15 11:32:49');
INSERT INTO `t_sys_menu` VALUES ('9', '添加角色', 'role:addBatch', '', '7', '1', '33', '', '2018-06-29 11:05:41', '2018-07-13 09:13:42');

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role` (
  ` ID ` VARCHAR (24
) NOT NULL COMMENT '角色id',
  `roleName` VARCHAR (20
) NOT NULL COMMENT '角色名称',
  `comments` VARCHAR (100
) DEFAULT NULL COMMENT '备注',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
PRIMARY KEY (` ID `
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE = 1 ROW_FORMAT = DYNAMIC COMMENT = '角色表';

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES ('1', '超级管理员', '超级管理员', '2018-07-21 09:58:31', '2018-07-21 11:07:16');
INSERT INTO `t_sys_role` VALUES ('2', '管理员', '系统管理员', '2018-07-21 09:58:35', '2018-07-21 11:07:18');
INSERT INTO `t_sys_role` VALUES ('3', '普通用户', '系统普通用户', '2018-07-21 09:58:39', '2018-07-21 11:07:23');

-- ----------------------------
-- Table structure for t_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_menu`;
CREATE TABLE `t_sys_role_menu` (
  `roleId` VARCHAR (24
) NOT NULL COMMENT '角色id',
  `menuId` VARCHAR (24
) NOT NULL COMMENT '权限id',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
KEY `sys_role_menu_roleId_index` (`roleId`
),
KEY `sys_role_menu_menuId_index` (`menuId`
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE = 1 ROW_FORMAT = DYNAMIC COMMENT = '角色菜单(权限)关联表';

-- ----------------------------
-- Records of t_sys_role_menu
-- ----------------------------
INSERT INTO `t_sys_role_menu` VALUES ('1', '1', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '2', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '3', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '4', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '5', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '6', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '7', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '8', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '9', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '10', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '11', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '12', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '13', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '14', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '15', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '16', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '17', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '18', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '19', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '20', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '23', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '24', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '26', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '27', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '29', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '30', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '31', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '32', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '28', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '33', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '34', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '35', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '36', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '37', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '38', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('1', '39', '2018-09-15 12:08:07');
INSERT INTO `t_sys_role_menu` VALUES ('3', '26', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '28', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '33', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '34', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '35', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '36', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '37', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '38', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('3', '39', '2018-09-16 19:19:19');
INSERT INTO `t_sys_role_menu` VALUES ('2', '51', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '52', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '53', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '1', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '54', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '2', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '3', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '4', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '5', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '6', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '7', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '8', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '9', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '10', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '11', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '12', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '13', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '14', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '15', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '16', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '17', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '18', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '19', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '20', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '21', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '22', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '40', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '41', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '42', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '43', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '44', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '45', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '46', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '47', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '48', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '49', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '50', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '23', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '24', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '55', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '26', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '27', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '29', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '30', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '31', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '32', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '28', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '33', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '34', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '35', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '36', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '37', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '38', '2018-10-18 10:25:35');
INSERT INTO `t_sys_role_menu` VALUES ('2', '39', '2018-10-18 10:25:35');

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user` (
  ` ID ` VARCHAR (24
) NOT NULL COMMENT '用户id',
  `username` VARCHAR (20
) NOT NULL COMMENT '账号',
  ` PASSWORD ` VARCHAR (128
) NOT NULL COMMENT '密码',
  `realName` VARCHAR (20
) DEFAULT NULL COMMENT '真实姓名',
  `nickName` VARCHAR (20
) NOT NULL COMMENT '昵称',
  `sex` VARCHAR (1
) NOT NULL DEFAULT '男' COMMENT '性别',
  `phone` VARCHAR (12
) DEFAULT NULL COMMENT '手机号',
  `avatar` VARCHAR (200
) DEFAULT NULL COMMENT '头像',
  `email` VARCHAR (100
) DEFAULT NULL COMMENT '邮箱',
  ` STATE ` INT (1
) NOT NULL DEFAULT '0' COMMENT '状态，0-正常，1-冻结',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `updateDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `officeId` VARCHAR (24
) DEFAULT NULL COMMENT '所属部门',
  `companyId` VARCHAR (24
) DEFAULT NULL COMMENT '所属公司',
PRIMARY KEY (` ID `
),
UNIQUE KEY `user_account` (`username`
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE = 1 ROW_FORMAT = DYNAMIC COMMENT = '用户表';

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES ('1', 'superAdmin', '$2a$04$zL8ODSgbRL.TQl9cIg5XUOiVevq4Ip2r/YJ1eXxODXnf9EaAQKvhu', NULL, '超级管理员', '男', '12345678901', NULL, NULL, '0', '2018-07-21 10:03:40', '2018-09-01 21:36:36', NULL, NULL);
INSERT INTO `t_sys_user` VALUES ('2', 'admin', '$2a$04$KY7D/USYlMptFiHDWO/K.OIfqnhRhI89iWiE.17zoyD1Ew8oGiism', 'Shaom', '管理员', '女', '12345678901', 'global/img/user_head.png', 'admin@lotso.com.cn', '0', '2018-07-21 10:50:18', '2018-09-01 11:13:40', '4', '1');
INSERT INTO `t_sys_user` VALUES ('3', 'Sam', '$2a$04$CnnjFZciwvm9gjw9Au.ZeOJRaJOKbIE.Sf/4dQdYKp4U06fQL5tCa', NULL, 'sam', '男', '13611111111', NULL, NULL, '0', '2018-09-13 09:33:33', '2018-09-13 09:33:33', '4', '3');

-- ----------------------------
-- Table structure for t_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_role`;
CREATE TABLE `t_sys_user_role` (
  `userId` VARCHAR (24
) NOT NULL COMMENT '用户id',
  `roleId` VARCHAR (24
) NOT NULL COMMENT '角色id',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP,
KEY `sys_user_role_userId_index` (`userId`
),
KEY `sys_user_role_roleId_index` (`roleId`
)
) ENGINE=InnoDB DEFAULT CHARSET =utf8mb4 CHECKSUM=1 DELAY_KEY_WRITE = 1 ROW_FORMAT = DYNAMIC COMMENT = '用户角色关联表';

-- ----------------------------
-- Records of t_sys_user_role
-- ----------------------------
INSERT INTO `t_sys_user_role` VALUES ('1', '1', '2018-07-18 15:25:50');
INSERT INTO `t_sys_user_role` VALUES ('3', '1', '2018-10-11 15:57:03');
INSERT INTO `t_sys_user_role` VALUES ('3', '2', '2018-10-11 15:57:03');
INSERT INTO `t_sys_user_role` VALUES ('3', '3', '2018-10-11 15:57:03');
INSERT INTO `t_sys_user_role` VALUES ('2', '2', '2018-10-11 16:35:42');
