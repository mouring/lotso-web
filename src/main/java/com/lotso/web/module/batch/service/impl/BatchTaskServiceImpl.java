package com.lotso.web.module.batch.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.common.utils.DateUtil;
import com.lotso.web.module.batch.entity.BatchSchedule;
import com.lotso.web.module.batch.entity.BatchTask;
import com.lotso.web.module.batch.mapper.IBatchTaskMapper;
import com.lotso.web.module.batch.service.IBatchTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * ClassName: BatchTaskServiceImpl
 * Description: 基本任务管理
 * Date: 2018/9/10 17:12 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BatchTaskServiceImpl extends ServiceImpl<IBatchTaskMapper, BatchTask> implements IBatchTaskService {

    @Autowired
    private IBatchTaskMapper taskMapper;


    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Result<BatchTask> list(ReqParam reqParam) {
        Wrapper<BatchTask> wrapper = new EntityWrapper<>();
        String searchValue = Strings.nullToEmpty(reqParam.getSearchValue()).trim();
        if (!Strings.isNullOrEmpty(searchValue)) {
            wrapper.andNew().eq("code", searchValue).or().like("name", searchValue);
        }

        Page<BatchTask> jobPage = new Page<>(reqParam.getPage(), reqParam.getLimit());
        List<BatchTask> jobList = taskMapper.selectPage(jobPage, wrapper);
        return new Result<>(jobPage.getTotal(), jobList);
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<BatchTask> list() {
        Wrapper<BatchTask> wrapper = new EntityWrapper<>();
        return taskMapper.selectList(wrapper.orderBy("createDate", true));
    }

    @Override
    public boolean add(BatchTask batchTask) {
        return taskMapper.insert(batchTask.initCode()) > 0;
    }

    @Override
    public boolean update(BatchTask batchTask) {
        batchTask.setUpdateDate(DateUtil.now());
        return taskMapper.updateById(batchTask) > 0;
    }

    @Autowired
    BatchScheduleServiceImpl scheduleService;

    @Override
    public boolean delete(String taskId) {
        BatchTask task = taskMapper.selectById(taskId);
        if (Objects.isNull(task)) {
            throw new BusinessException("没有查询到该任务");
        }

        //判断改任务下面有没有计划
        if (scheduleService.selectCount(new EntityWrapper<BatchSchedule>().eq("taskCode", task.getCode())) > 0) {
            throw new BusinessException("该任务下有任务计划，不能删除");
        }

        return taskMapper.deleteById(taskId) > 0;
    }
}
