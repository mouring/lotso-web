package com.lotso.web.module.batch.job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * ClassName: AbstractJobBean
 * Description: 作业抽象类（有状态的）
 * Date: 2018/9/6 17:27 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Slf4j
@DisallowConcurrentExecution
public class AbstractJobBean extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        this.doData(context);
    }

    /**
     * 具体执行计划业务处理类
     *
     * @param context
     * @throws JobExecutionException
     */
    protected void doData(JobExecutionContext context) throws JobExecutionException {
    }
}
