package com.lotso.web.common.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * ClassName: QuartzConfigTest
 * Description:
 * Date: 2018/9/10 10:03 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class QuartzConfigTest {


    @Test
    public void test() {

    }
}