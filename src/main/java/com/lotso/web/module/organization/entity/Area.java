package com.lotso.web.module.organization.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractDataEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: Area
 * Description: 区域
 * Date: 2018/9/27 18:33 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */

@Getter
@Setter
@TableName("t_org_area")
public class Area extends AbstractDataEntity {

    /**
     * 区域编码
     */
    private String code;
    /**
     * 区域名称
     */
    private String name;

    /**
     * 区域类型（1：国家；2：省份、直辖市；3：地市；4：区县）
     */
    private String type;

    private String parentId;

    /**
     * 排序
     */
    private Integer sort;

}
