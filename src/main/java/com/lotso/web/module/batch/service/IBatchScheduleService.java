package com.lotso.web.module.batch.service;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.common.exception.ParameterException;
import com.lotso.web.module.batch.entity.BatchSchedule;

import java.util.List;

/**
 * ClassName: IBatchScheduleService
 * Description: 任务计划
 * Date: 2018/9/10 13:28 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IBatchScheduleService {

    /**
     * 分页查询
     *
     * @param reqParam
     * @return
     */
    Result<BatchSchedule> list(ReqParam reqParam);

    /**
     * 列表
     *
     * @return
     */
    List<BatchSchedule> list();

    /**
     * 添加
     *
     * @param batchSchedule
     * @return
     */
    boolean add(BatchSchedule batchSchedule) throws BusinessException;

    /**
     * 修改
     *
     * @param batchSchedule
     * @return
     */
    boolean update(BatchSchedule batchSchedule) throws ParameterException;

    /**
     * 删除
     *
     * @param scheduleId
     * @return
     */
    boolean delete(String scheduleId);

    /**
     * 启动一个计划
     *
     * @param scheduleId
     * @return
     */
    boolean run(String scheduleId);

    /**
     * 暂停一个计划
     *
     * @param scheduleId
     * @return
     */
    boolean pause(String scheduleId);

    /**
     * 恢复运行一个计划
     *
     * @param scheduleId
     * @return
     */
    boolean resume(String scheduleId);

    /**
     * 唤醒子计划
     *
     * @param scheduleJob
     */
    void notifyChildren(BatchSchedule scheduleJob);
}
