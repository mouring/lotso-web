package com.lotso.web.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * ClassName: ApiResult
 * Description: API 结果类
 * Date: 2018/9/2 7:38 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 * @see HttpStatus
 */
@Getter
@Setter
public class ApiResult<T> {

    public static final boolean SUCCESS = true;
    public static final boolean FAILURE = false;

    /**
     * 状态
     */
    private boolean success = SUCCESS;

    /**
     * 状态码，建议与 HttpStatus 状态码保持一致
     */
    private int code;

    /**
     * 提示信息
     */
    private String msg;


    /**
     * 当前数据
     */
    private List<T> dataList;

    /**
     * 当前数据
     */
    private T data;


    public ApiResult() {
    }


    public ApiResult(T data) {
        this.code = HttpStatus.OK.value();
        this.data = data;
    }

    public ApiResult(boolean success, int code, String msg) {
        this.success = success;
        this.code = code;
        this.msg = msg;
    }

    public ApiResult(boolean success, int code, String msg, List<T> dataList) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.dataList = dataList;
    }

    public ApiResult(boolean success, int code, String msg, T data) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static ApiResult ok() {
        return ok("操作成功");
    }

    public static ApiResult ok(String msg) {
        return ok(HttpStatus.OK.value(), msg);
    }

    public static ApiResult ok(int code, String msg) {
        return new ApiResult(SUCCESS, code, msg);
    }

    public static ApiResult error() {
        return error("操作失败");
    }

    public static ApiResult error(String msg) {
        return error(500, msg);
    }

    public static ApiResult error(int code, String msg) {
        return new ApiResult(FAILURE, code, msg);
    }


}
