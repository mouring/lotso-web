<html>
<head>
    <title>${title}</title>
</head>
<body>
<h1>PDF TEMPLATE PAGE - PDF模板</h1>
<div>
    <div align="center">
        <h1>${title}</h1>
    </div>
    <table>
        <tr>
            <td><b>Name</b></td>
            <td><b>Age</b></td>
            <td><b>Sex</b></td>
        </tr>
        <tr>
            <td>${name}</td>
            <td>${age}</td>
            <td><#if sex = 1> male <#else> female </#if></td>
        </tr>
    </table>
</div>
<div>
    <a href="${url}" target="_blank">Sam博客</a>
</div>
<div>
    <img src="${img}"/>
</div>
</body>
</html>