package com.lotso.web.module.api;

import com.lotso.web.common.ApiResult;
import com.lotso.web.module.base.AbstractController;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: ApiSwaggerController
 * Description: 简单的例子
 * Date: 2019/4/24 16:40 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RestController
@RequestMapping("/api")
public class ApiSwaggerController extends AbstractController {

    @ApiOperation(value = "简单的例子", notes = "简单的例子")
    @GetMapping("/get")
    public ApiResult apiGet() {
        return ApiResult.ok("ApiSwaggerController API 接口访问");
    }
}
