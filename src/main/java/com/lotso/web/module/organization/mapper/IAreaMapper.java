package com.lotso.web.module.organization.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.organization.entity.Area;
import org.springframework.stereotype.Repository;

/**
 * ClassName: IAreaMapper
 * Description: 区域
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IAreaMapper extends IBaseMapper<Area> {

}
