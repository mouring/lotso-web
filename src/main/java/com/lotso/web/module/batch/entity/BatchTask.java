package com.lotso.web.module.batch.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.google.common.base.Strings;
import com.lotso.web.module.base.AbstractDataEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * ClassName: BatchTask
 * Description: 基本任务管理
 * 基本任务（BatchTask）与任务计划（BatchSchedule）是一对多关系，在 Quartz 代替 jobGroup 的概念
 * Date: 2018/9/6 14:14 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@TableName("t_batch_task")
public class BatchTask extends AbstractDataEntity {

    public static final String DEFAULT_CODE_PREFIX = "BT";

    /**
     * 任务编码：唯一
     */
    private String code;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务描述
     */
    private String description;

    /**
     * 前置任务
     */
    @TableField(exist = false)
    private List<BatchTask> previous;


    public BatchTask initCode() {
        this.code = Strings.isNullOrEmpty(this.code) ? (DEFAULT_CODE_PREFIX + IdWorker.getIdStr()) : this.code;
        return this;
    }
}
