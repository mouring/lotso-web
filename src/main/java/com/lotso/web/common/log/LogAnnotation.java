package com.lotso.web.common.log;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: LogAnnotation
 * Description: 日志记录的注解
 * Date: 2017/10/25 10:44
 *
 * @author Shaom
 * @version V1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LogAnnotation {

    String desc();

    MethodType type();

    enum MethodType {
        /**
         * 新增
         */
        CREATE("新增"),
        /**
         * 访问
         */
        READ("访问"),
        /**
         * 修改
         */
        UPDATE("修改"),
        /**
         * 删除
         */
        DELETE("删除");
        private String desc;

        MethodType(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }
}
