package com.lotso.web.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * ClassName: Result
 * Description: 结果类
 * Date: 2018/9/2 7:38 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 * @see org.springframework.http.HttpStatus
 */
@Getter
@Setter
public class Result<T> {

    public static final boolean SUCCESS = true;
    public static final boolean FAILURE = false;

    /**
     * 表单渲染的时候，成功返回code为0
     */
    public static final int FORM_SUCCESS = 0;

    /**
     * 状态
     */
    private boolean success = SUCCESS;

    /**
     * 状态码，建议与 HttpStatus 状态码保持一致
     */
    private int code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 总数量
     * 命名以 layui 框架的 table 为标准 ， bootstrapTable 是 total
     */
    private long count;

    /**
     * 当前数据,
     * 命名以 layui 框架的 table 为标准，bootstrapTable 是 rows
     */
    private List<T> data;

    /**
     * 当前数据
     */
    private T resData;


    public Result() {
    }


    public Result(T resData) {
        this.code = HttpStatus.OK.value();
        this.resData = resData;
    }

    public Result(boolean success, int code, String msg) {
        this.success = success;
        this.code = code;
        this.msg = msg;
    }

    public Result(List<T> data) {
        this(data.size(), data);
    }

    public Result(long count, List<T> data) {
        this(FORM_SUCCESS, null, count, data);
    }

    private Result(int code, String msg, long count, List<T> data) {
        this(SUCCESS, code, msg, count, data);
    }

    public Result(boolean success, int code, String msg, long count, List<T> data) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }

    public static Result ok() {
        return ok("操作成功");
    }

    public static Result ok(String msg) {
        return ok(HttpStatus.OK.value(), msg);
    }

    public static Result ok(int code, String msg) {
        return new Result(SUCCESS, code, msg);
    }

    public static Result error() {
        return error("操作失败");
    }

    public static Result error(String msg) {
        return error(500, msg);
    }

    public static Result error(int code, String msg) {
        return new Result(FAILURE, code, msg);
    }


}
