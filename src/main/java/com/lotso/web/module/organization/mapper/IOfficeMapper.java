package com.lotso.web.module.organization.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.organization.entity.Office;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: IOfficeMapper
 * Description: 机构
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IOfficeMapper extends IBaseMapper<Office> {

    /**
     * 获取用户有权限的机构信息（所属机构以及下级机构）
     *
     * @param username
     * @param type
     * @return
     */
    List<Office> authGet(@Param("username") String username, @Param("type") String type);

    /**
     * 获取用户有权限的机构信息（所属机构以及下级机构）（Oracle）
     *
     * @param username
     * @param type
     * @return
     */
    List<Office> authGetForOracle(@Param("username") String username, @Param("type") String type);
}
