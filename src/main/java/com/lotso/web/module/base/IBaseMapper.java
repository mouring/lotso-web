package com.lotso.web.module.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * ClassName: IBaseMapper
 * Description: 基础 Mapper
 * Date: 2018/9/2 9:09 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IBaseMapper<T> extends BaseMapper<T> {
}
