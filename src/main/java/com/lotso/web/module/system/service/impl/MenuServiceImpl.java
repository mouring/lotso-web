package com.lotso.web.module.system.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.common.utils.DateUtil;
import com.lotso.web.module.system.entity.Menu;
import com.lotso.web.module.system.entity.RoleMenu;
import com.lotso.web.module.system.mapper.IMenuMapper;
import com.lotso.web.module.system.mapper.IRoleMenuMapper;
import com.lotso.web.module.system.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ClassName: MenuServiceImpl
 * Description: 菜单业务处理
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MenuServiceImpl extends ServiceImpl<IMenuMapper, Menu> implements IMenuService {

    @Autowired
    private IMenuMapper menuMapper;
    @Autowired
    private IRoleMenuMapper roleMenuMapper;

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Menu> listByUserId(String userId) {
        return menuMapper.listByUserId(userId);
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Menu> list() {
        return menuMapper.selectList(new EntityWrapper<Menu>().orderBy("sort", true));
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Menu> listMenu() {

        Wrapper<Menu> wrapper = new EntityWrapper<Menu>()
                .eq("isMenu", Menu.IS_MENU)
                .orderBy("sort", true);
        return menuMapper.selectList(wrapper);
    }


    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Menu> listByRoleId(String roleId) {
        return menuMapper.listByRoleId(roleId);
    }

    @Override
    public boolean add(Menu menu) {
        return menuMapper.insert(menu) > 0;
    }

    @Override
    public boolean update(Menu menu) {
        menu.setUpdateDate(DateUtil.now());
        return menuMapper.updateById(menu) > 0;
    }


    @Override
    public boolean delete(String menuId) {
        List<Menu> children = menuMapper.selectList(new EntityWrapper<Menu>().eq("parentId", menuId));
        if (children != null && children.size() > 0) {
            throw new BusinessException("请先删除子节点");
        }

        boolean res = menuMapper.deleteById(menuId) > 0;
        if (res) {
            if (roleMenuMapper.delete(new EntityWrapper<RoleMenu>().eq("menuId", menuId)) < 0) {
                throw new BusinessException("修改失败，请重试");
            }
        }
        return res;
    }

    @Override
    public boolean updateRoleAuth(String roleId, List<String> menuIds) {
        roleMenuMapper.delete(new EntityWrapper<RoleMenu>().eq("roleId", roleId));
        if (menuIds != null && menuIds.size() > 0) {
            if (roleMenuMapper.insertRoleAuths(roleId, menuIds) < menuIds.size()) {
                throw new BusinessException("操作失败");
            }
        }
        return true;
    }


}
