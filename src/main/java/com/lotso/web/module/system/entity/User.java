package com.lotso.web.module.system.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractDataEntity;
import com.lotso.web.module.organization.entity.Office;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * ClassName: User
 * Description: 用户表
 * Date: 2018/9/2 7:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@TableName("t_sys_user")
public class User extends AbstractDataEntity {

    public static final String ADMIN = "admin";
    public static final String SUPER_ADMIN = "superAdmin";
    /**
     * 默认密码
     */
    public static final String DEFAULT_PWD = "123456";


    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称（用户名）
     */
    private String nickName;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别
     */
    private String sex;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 角色
     */
    @TableField(exist = false)
    private List<Role> roles;

    /**
     * 所属公司
     */
    @TableField(exist = false)
    private String companyName;
    private String companyId;

    /**
     * 所属部门
     */
    @TableField(exist = false)
    private String officeName;
    private String officeId;
}
