package com.lotso.web.module.system.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.module.system.entity.Role;
import com.lotso.web.module.system.entity.RoleMenu;
import com.lotso.web.module.system.entity.UserRole;
import com.lotso.web.module.system.mapper.IRoleMapper;
import com.lotso.web.module.system.mapper.IRoleMenuMapper;
import com.lotso.web.module.system.mapper.IUserRoleMapper;
import com.lotso.web.module.system.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * ClassName: RoleServiceImpl
 * Description: 角色业务处理
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RoleServiceImpl extends ServiceImpl<IRoleMapper, Role> implements IRoleService {

    @Autowired
    private IRoleMapper roleMapper;
    @Resource
    private IUserRoleMapper userRoleMapper;
    @Autowired
    private IRoleMenuMapper roleMenuMapper;

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Role> getByUserId(String userId) {
        return roleMapper.selectByUserId(userId);
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Role> list() {
        Wrapper<Role> wrapper = new EntityWrapper<>();
        return roleMapper.selectList(wrapper.orderBy("createDate", true));
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Result<Role> list(ReqParam reqParam) {
        Wrapper<Role> wrapper = new EntityWrapper<>();
        // 拼接参数
        String searchValue = Strings.nullToEmpty(reqParam.getSearchValue()).trim();
        if (!Strings.isNullOrEmpty(searchValue)) {
            wrapper.andNew().eq("roleName", searchValue).or().like("comments", searchValue);
        }

        // 分页查询
        Page<Role> rolePage = new Page<>(reqParam.getPage(), reqParam.getLimit());
        List<Role> roleList = roleMapper.selectPage(rolePage, wrapper.orderBy("createDate", true));
        return new Result<>(rolePage.getTotal(), roleList);
    }

    @Override
    public boolean add(Role role) {
        return roleMapper.insert(role) > 0;
    }

    @Override
    public boolean update(Role role) {
        return roleMapper.updateById(role) > 0;
    }


    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Role getById(String roleId) {
        return roleMapper.selectById(roleId);
    }

    @Override
    public boolean delete(String roleId) {
        // 这个角色有分配给用户，这不能删除
        if (userRoleMapper.selectCount(new EntityWrapper<UserRole>().eq("roleId", roleId)) > 0) {
            throw new BusinessException("该角色下有分配用户，不能删除");
        }

        boolean rs = roleMapper.deleteById(roleId) > 0;

        // 删除角色的权限信息
        if (rs) {
            if (roleMenuMapper.delete(new EntityWrapper<RoleMenu>().eq("roleId", roleId)) < 0) {
                throw new BusinessException("删除角色已分配权限失败，请重试");
            }
        }
        return rs;
    }
}
