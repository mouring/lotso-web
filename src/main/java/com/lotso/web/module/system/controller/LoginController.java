package com.lotso.web.module.system.controller;

import com.lotso.web.common.Result;
import com.lotso.web.module.base.AbstractController;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * ClassName: LoginController
 * Description: 登录控制器
 * Date: 2018/9/1 7:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
public class LoginController extends AbstractController {

    /**
     * 图形验证码
     */
    @RequestMapping("/api/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws IOException {
        CaptchaUtil.out(4, request, response);
    }

    /**
     * Session 超时
     *
     * @return
     * @throws IOException
     */
    @RequestMapping("/api/ajax/authentication")
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Result invalid() throws IOException {
        return Result.error(HttpStatus.UNAUTHORIZED.value(), "Session 过期");
    }


}
