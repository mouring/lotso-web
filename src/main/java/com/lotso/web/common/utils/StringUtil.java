package com.lotso.web.common.utils;

import com.baomidou.mybatisplus.toolkit.ArrayUtils;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

import java.util.Arrays;
import java.util.List;

/**
 * ClassName: StringUtil
 * Description: 字符串工具类
 * 一般的功能，使用{@link Strings}，也支持本类进行扩展
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class StringUtil {


    private static final Splitter SPLITTER = Splitter.on(",");

    public static List<String> splitToList(CharSequence sequence) {
        return SPLITTER.splitToList(sequence);
    }


    /**
     * 没有一个是空值或null
     *
     * @param string
     * @return
     */
    public static boolean hasNoneNullOrEmpty(String... string) {
        if (ArrayUtils.isEmpty(string)) {
            return false;
        } else {
            return Arrays.stream(string).noneMatch(Strings::isNullOrEmpty);
        }
    }

    /**
     * 有空值或null
     *
     * @param string
     * @return
     */
    public static boolean hasNullOrEmpty(String... string) {
        if (ArrayUtils.isEmpty(string)) {
            return true;
        } else {
            return Arrays.stream(string).anyMatch(Strings::isNullOrEmpty);
        }
    }


}
