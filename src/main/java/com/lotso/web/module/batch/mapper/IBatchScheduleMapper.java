package com.lotso.web.module.batch.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.lotso.web.common.ReqParam;
import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.batch.entity.BatchSchedule;
import com.lotso.web.module.batch.entity.BatchScheduleParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: IBatchScheduleMapper
 * Description:
 * Date: 2018/9/10 13:45 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IBatchScheduleMapper extends IBaseMapper<BatchSchedule> {

    /**
     * 根据ID 获取计划任务
     *
     * @param scheduleId
     * @return
     */
    BatchSchedule findById(String scheduleId);

    /**
     * 分页展示
     *
     * @param jobPage
     * @param reqParam
     * @return
     */
    List<BatchSchedule> listByPage(Page<BatchSchedule> jobPage, ReqParam reqParam);

    /**
     * 分页展示（Oracle）
     *
     * @param jobPage
     * @param reqParam
     * @return
     */
    List<BatchSchedule> listByPageForOracle(Page<BatchSchedule> jobPage, ReqParam reqParam);


    /**
     * 批量保存计划参数
     *
     * @param scheduleId
     * @param scheduleCode
     * @param params
     * @return
     */
    int insertBatchParams(@Param("scheduleId") String scheduleId, @Param("scheduleCode") String scheduleCode, @Param("params") List<BatchScheduleParam> params);

    /**
     * 批量保存计划参数（Oracle）
     *
     * @param scheduleId
     * @param scheduleCode
     * @param params
     * @return
     */
    int insertBatchParamsForOracle(@Param("scheduleId") String scheduleId, @Param("scheduleCode") String scheduleCode, @Param("params") List<BatchScheduleParam> params);

    /**
     * 批量保存依赖计划（前置计划）
     *
     * @param scheduleId
     * @param previous
     * @return
     */
    int insertBatchDependencies(@Param("scheduleId") String scheduleId, @Param("dependencies") List<BatchSchedule> previous);

    /**
     * 批量保存依赖计划（前置计划）（Oracle）
     *
     * @param scheduleId
     * @param previous
     * @return
     */
    int insertBatchDependenciesForOracle(@Param("scheduleId") String scheduleId, @Param("dependencies") List<BatchSchedule> previous);


    /**
     * 删除依赖计划（前置计划）
     *
     * @param scheduleId
     * @return
     */
    int deleteDependencies(@Param("scheduleId") String scheduleId);

    /**
     * 获取依赖于该计划的子计划
     *
     * @param scheduleId
     * @return
     */
    List<BatchSchedule> findChildrenSchedule(@Param("scheduleId") String scheduleId);
}
