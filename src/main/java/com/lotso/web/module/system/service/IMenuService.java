package com.lotso.web.module.system.service;

import com.lotso.web.module.system.entity.Menu;

import java.util.List;

/**
 * ClassName: AuthoritiesService
 * Description: 权限业务
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IMenuService {

    /**
     * 获取登录用户菜单信息
     *
     * @param userId
     * @return
     */
    List<Menu> listByUserId(String userId);

    /**
     * 查询所有的菜单列表
     *
     * @return
     */
    List<Menu> list();

    /**
     * 查询所有为【isMenu】的菜单
     *
     * @return
     */
    List<Menu> listMenu();


    /**
     * 根据角色ID 获取该角色的权限列表
     *
     * @param roleId
     * @return
     */
    List<Menu> listByRoleId(String roleId);

    /**
     * 添加菜单
     *
     * @param menu
     * @return
     */
    boolean add(Menu menu);

    /**
     * 更新菜单
     *
     * @param menu
     * @return
     */
    boolean update(Menu menu);

    /**
     * 删除菜单
     *
     * @param menuId
     * @return
     */
    boolean delete(String menuId);

    /**
     * 批量更新角色分配的权限列表（角色分配权限时调用）
     *
     * @param roleId
     * @param menuIds
     * @return
     */
    boolean updateRoleAuth(String roleId, List<String> menuIds);
}
