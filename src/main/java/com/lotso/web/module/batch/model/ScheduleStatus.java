package com.lotso.web.module.batch.model;

/**
 * ClassName: ScheduleStatus
 * Description: 计划状态
 * 参考 {@link org.quartz.Trigger.TriggerState}
 * Date: 2018/9/13 10:09 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class ScheduleStatus {

    /**
     * 未启用（初始化状态）
     */
    public static final int NONE = 0;
    /**
     * 等待运行（加入队列，未运行）
     */
    public static final int NORMAL = 1;

    /**
     * 暂停
     */
    public static final int PAUSED = 2;

    /**
     * 运行完成（正确运行）
     */
    public static final int COMPLETE = 3;

    /**
     * 异常失败
     */
    public static final int ERROR = 4;

    /**
     * 运行阻塞
     */
    public static final int BLOCKED = 5;

    /**
     * 启用，未启动（介于0~1之间的状态）
     */
    public static final int ENABLED = 6;

    /**
     * 运行中（介于1~3之间的状态）
     */
    public static final int RUNNING = 7;
}
