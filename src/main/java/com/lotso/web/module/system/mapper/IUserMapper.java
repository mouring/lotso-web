package com.lotso.web.module.system.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.lotso.web.common.ReqParam;
import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.system.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: IUserMapper
 * Description: 用户
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IUserMapper extends IBaseMapper<User> {

    /**
     * 通过账号获取用户信息
     *
     * @param username
     * @return
     */
    User getDetailByUsername(String username);

    /**
     * 分页查询
     *
     * @param userPage
     * @param reqParam
     * @return
     */
    List<User> listByPage(Page<User> userPage, ReqParam reqParam);

    /**
     * 分页查询（Oracle）
     *
     * @param userPage
     * @param reqParam
     * @return
     */
    List<User> listByPageForOracle(Page<User> userPage, ReqParam reqParam);
}
