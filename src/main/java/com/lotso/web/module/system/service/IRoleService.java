package com.lotso.web.module.system.service;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.module.system.entity.Role;

import java.util.List;

/**
 * ClassName:
 * Description:
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IRoleService {

    /**
     * 通过用户ID获取其用户所有的角色列表
     *
     * @param userId
     * @return
     */
    List<Role> getByUserId(String userId);

    /**
     * 查询角色列表
     *
     * @return
     */
    List<Role> list();

    /**
     * 分页查询角色列表
     *
     * @param reqParam
     * @return
     */
    Result<Role> list(ReqParam reqParam);

    /**
     * 通过角色ID 角色信息
     *
     * @param roleId
     * @return
     */
    Role getById(String roleId);

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    boolean add(Role role);

    /**
     * 修改角色
     *
     * @param role
     * @return
     */
    boolean update(Role role);

    /**
     * 删除角色（物理删除）
     *
     * @param roleId
     * @return
     */
    boolean delete(String roleId);

}
