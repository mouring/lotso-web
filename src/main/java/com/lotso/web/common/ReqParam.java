package com.lotso.web.common;

import com.lotso.web.common.log.LogEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * ClassName: ReqParam
 * Description: 请求参数对象
 * Date: 2018/9/3 17:19 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@LogEntity
public class ReqParam {

    /**
     * 第几页（pageNum）
     */
    private int page = 0;

    /**
     * 每页显示条数（pageSize）
     */
    private int limit = 10;

    /**
     * 开始时间
     */
    private String startDate;
    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 查询 key
     */
    private String searchKey;

    /**
     * 查询 value
     */
    private String searchValue;

    /**
     * 查询参数
     */
    private Map<String, String> params;
}
