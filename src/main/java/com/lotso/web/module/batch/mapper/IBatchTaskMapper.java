package com.lotso.web.module.batch.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.batch.entity.BatchTask;
import org.springframework.stereotype.Repository;

/**
 * ClassName: IBatchTaskMapper
 * Description: 基本任务
 * Date: 2018/9/10 13:45 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IBatchTaskMapper extends IBaseMapper<BatchTask> {
}
