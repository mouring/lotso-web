package com.lotso.web.module.system.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.system.entity.UserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: IUserRoleMapper
 * Description: 用户角色关系
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IUserRoleMapper extends IBaseMapper<UserRole> {

    /**
     * 通过用户ID 获取全部角色信息
     *
     * @param userIds
     * @return
     */
    List<UserRole> selectByUserIds(@Param("userIds") List<String> userIds);

    /**
     * 为ID 为 userId 的用户批量分配角色
     *
     * @param userId
     * @param roleIds
     * @return
     */
    int insertBatch(@Param("userId") String userId, @Param("roleIds") List<String> roleIds);

    /**
     * 为ID 为 userId 的用户批量分配角色（Oracle）
     *
     * @param userId
     * @param roleIds
     * @return
     */
    int insertBatchForOracle(@Param("userId") String userId, @Param("roleIds") List<String> roleIds);
}
