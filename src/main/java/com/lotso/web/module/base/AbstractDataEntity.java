package com.lotso.web.module.base;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * ClassName: AbstractDataEntity
 * Description: Entity 基类
 * Date: 2018/9/2 7:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
public abstract class AbstractDataEntity extends AbstractBaseEntity {

    /**
     * 创建日期
     */
    protected Date createDate;

    /**
     * 更新日期
     */
    protected Date updateDate;

    public AbstractDataEntity() {
    }

    public AbstractDataEntity(String id) {
        super(id);
    }
}
