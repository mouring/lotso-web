package com.lotso.web.module.organization.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.module.organization.entity.Area;
import com.lotso.web.module.organization.mapper.IAreaMapper;
import com.lotso.web.module.organization.service.IAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ClassName: AreaServiceImpl
 * Description:
 * Date: 2018/9/27 18:47 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AreaServiceImpl extends ServiceImpl<IAreaMapper, Area> implements IAreaService {

    @Autowired
    private IAreaMapper areaMapper;

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public List<Area> list() {
        return areaMapper.selectList(new EntityWrapper<Area>().orderBy("sort", true));
    }

    @Override
    public boolean add(Area area) {
        return areaMapper.insert(area) > 0;
    }

    @Override
    public boolean update(Area area) {
        return areaMapper.updateById(area) > 0;
    }

    @Override
    public boolean delete(String areaId) {
        List<Area> children = areaMapper.selectList(new EntityWrapper<Area>().eq("parentId", areaId));
        if (children != null && children.size() > 0) {
            throw new BusinessException("请先删除子节点");
        }
        return areaMapper.deleteById(areaId) > 0;
    }
}