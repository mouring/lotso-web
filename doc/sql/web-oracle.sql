CREATE TABLE T_BATCH_SCHEDULE
(
  id              VARCHAR2(24) PRIMARY KEY,
  code            VARCHAR2(32)         NOT NULL,
  name            VARCHAR2(64)         NOT NULL,
  status          NUMBER(1) DEFAULT 0  NOT NULL,
  cron_type       NUMBER(1) DEFAULT '1',
  cron_expression VARCHAR2(64),
  interfacename   VARCHAR2(255),
  server_ip       VARCHAR2(64),
  description     VARCHAR2(255),
  task_code       VARCHAR2(32)         NOT NULL,
  start_date      DATE,
  end_date        DATE,
  create_date     DATE DEFAULT sysdate NOT NULL,
  update_date     DATE DEFAULT sysdate NOT NULL,
  CONSTRAINT T_BATCH_SCHEDULE_CODE UNIQUE (code)
)

-- Add comments to the columns 
COMMENT ON COLUMN T_BATCH_SCHEDULE.code
IS '计划编码';
COMMENT ON COLUMN T_BATCH_SCHEDULE.name
IS '计划名称';
COMMENT ON COLUMN T_BATCH_SCHEDULE.status
IS '计划状态';
COMMENT ON COLUMN T_BATCH_SCHEDULE.cron_type
IS '表达式类型，默认为 1-执行一次';
COMMENT ON COLUMN T_BATCH_SCHEDULE.cron_expression
IS '执行表达式';
COMMENT ON COLUMN T_BATCH_SCHEDULE.server_ip
IS '运行服务器Ip';
COMMENT ON COLUMN T_BATCH_SCHEDULE.description
IS '描述';
COMMENT ON COLUMN T_BATCH_SCHEDULE.start_date
IS '最近开始时间';
COMMENT ON COLUMN T_BATCH_SCHEDULE.end_date
IS '最近结束时间';

)
------------------------------------------------------
INSERT INTO `t_batch_schedule` VALUES ('1', 'BS1040795600922087426', '计划测试-01', '3', '1', ' ', 'com.sinosoft.web.module.batch.job.impl.SimpleJob', '127.0.0.1', '基本任务-计划测试', 'BT1039705122717872130', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));

------------------------------------------------------
-- Create table
CREATE TABLE T_BATCH_SCHEDULE_PARAM
(
  schedule_id   VARCHAR2(24),
  schedule_code VARCHAR2(32),
  param_name    VARCHAR2(32),
  param_value   VARCHAR2(32),
  temp_id       VARCHAR2(64)
)
-- Add comments to the table 
COMMENT ON TABLE T_BATCH_SCHEDULE_PARAM
IS '任务计划参数';
-- Add comments to the columns 
COMMENT ON COLUMN T_BATCH_SCHEDULE_PARAM.schedule_id
IS '任务计划Id';
COMMENT ON COLUMN T_BATCH_SCHEDULE_PARAM.schedule_code
IS '任务计划Code';
COMMENT ON COLUMN T_BATCH_SCHEDULE_PARAM.param_name
IS '参数名';
COMMENT ON COLUMN T_BATCH_SCHEDULE_PARAM.param_value
IS '参数值';
COMMENT ON COLUMN T_BATCH_SCHEDULE_PARAM.temp_id
IS '参数描述';
-------------------------------------------------------
INSERT INTO t_batch_schedule_param
VALUES ('1', 'BS1040795600922087426', 'name', 'sam', '1537096814399');
INSERT INTO t_batch_schedule_param
VALUES ('1', 'BS1040795600922087426', 'address', 'suzhou', '1537096837591');
------------------------------------------------------
-- Create table
CREATE TABLE T_BATCH_TASK
(
  id          VARCHAR2(24) PRIMARY KEY,
  code        VARCHAR2(32)         NOT NULL,
  name        VARCHAR2(64)         NOT NULL,
  description VARCHAR2(128),
  create_date DATE DEFAULT sysdate NOT NULL,
  update_date DATE DEFAULT sysdate NOT NULL,
  CONSTRAINT T_BATCH_TASK_CODE UNIQUE (code)
)

-- Add comments to the table 
COMMENT ON TABLE T_BATCH_TASK
IS '批量任务组';
-- Add comments to the columns 
COMMENT ON COLUMN T_BATCH_TASK.code
IS '任务编码';
COMMENT ON COLUMN T_BATCH_TASK.name
IS '任务名称';
COMMENT ON COLUMN T_BATCH_TASK.description
IS '任务描述';
-----------------------------------------------------
INSERT INTO t_batch_task
VALUES ('1', 'BT1039705122717872130', '基础任务', '基础任务', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
------------------------------------------------------
-- Create table
CREATE TABLE T_SYS_DICT
(
  id          VARCHAR2(24) PRIMARY KEY,
  value       VARCHAR2(100) NOT NULL,
  label       VARCHAR2(100) NOT NULL,
  type        VARCHAR2(100) NOT NULL,
  description VARCHAR2(100) NOT NULL,
  sort        NUMBER(10)    NOT NULL,
  parent_id   VARCHAR2(64) DEFAULT '0',
  create_date DATE         DEFAULT sysdate,
  update_date DATE         DEFAULT sysdate
)
-- Add comments to the table 
COMMENT ON TABLE T_SYS_DICT
IS '字典表';
-- Add comments to the columns 
COMMENT ON COLUMN T_SYS_DICT.id
IS '编号';
COMMENT ON COLUMN T_SYS_DICT.value
IS '数据值';
COMMENT ON COLUMN T_SYS_DICT.label
IS '标签名';
COMMENT ON COLUMN T_SYS_DICT.type
IS '类型';
COMMENT ON COLUMN T_SYS_DICT.description
IS '描述';
COMMENT ON COLUMN T_SYS_DICT.sort
IS '排序（升序）';
COMMENT ON COLUMN T_SYS_DICT.parent_id
IS '父级编号';
COMMENT ON COLUMN T_SYS_DICT.create_date
IS '创建时间';
COMMENT ON COLUMN T_SYS_DICT.update_date
IS '更新时间';
-------------------------------------------------
INSERT INTO t_sys_dict
VALUES ('1', '0', '未启用', 'ScheduleStatus', '计划状态', '10', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('10', '2', '每分钟一次', 'CronType', '执行表达式类型', '20', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('11', '3', '每小时一次', 'CronType', '执行表达式类型', '30', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('12', '4', '每天一次', 'CronType', '执行表达式类型', '40', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('13', '5', '每月一次', 'CronType', '执行表达式类型', '50', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('14', '6', '每周一次', 'CronType', '执行表达式类型', '60', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('15', '0', '其他', 'CronType', '执行表达式类型', '70', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('2', '1', '正常启动中', 'ScheduleStatus', '计划状态', '20', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('3', '2', '暂停', 'ScheduleStatus', '计划状态', '30', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('4', '3', '运行完成', 'ScheduleStatus', '计划状态', '40', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('5', '4', '异常失败', 'ScheduleStatus', '计划状态', '50', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('6', '5', '运行阻塞', 'ScheduleStatus', '计划状态', '60', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('7', '6', '启用，未启动', 'ScheduleStatus', '计划状态', '70', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('8', '7', '运行中', 'ScheduleStatus', '计划状态', '80', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_dict
VALUES ('9', '1', '执行一次', 'CronType', '执行表达式类型', '10', '0', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));

------------------------------------------------

-- Create table
CREATE TABLE T_SYS_LOG
(
  id           VARCHAR2(24) PRIMARY KEY,
  user_name    VARCHAR2(20)         NOT NULL,
  url          VARCHAR2(40),
  action       VARCHAR2(10),
  args         VARCHAR2(1024),
  exception    VARCHAR2(2048),
  osname       VARCHAR2(24),
  device       VARCHAR2(24),
  browser_type VARCHAR2(40),
  ip           VARCHAR2(40),
  create_time  DATE DEFAULT sysdate NOT NULL
)

-- Add comments to the columns 
COMMENT ON COLUMN T_SYS_LOG.id
IS '主键';
COMMENT ON COLUMN T_SYS_LOG.user_name
IS '用户账号';
COMMENT ON COLUMN T_SYS_LOG.url
IS '访问地址';
COMMENT ON COLUMN T_SYS_LOG.action
IS '操作动作';
COMMENT ON COLUMN T_SYS_LOG.args
IS '具体参数';
COMMENT ON COLUMN T_SYS_LOG.exception
IS '异常信息';
COMMENT ON COLUMN T_SYS_LOG.osname
IS '操作系统';
COMMENT ON COLUMN T_SYS_LOG.device
IS '设备型号';
COMMENT ON COLUMN T_SYS_LOG.browser_type
IS '浏览器类型';
COMMENT ON COLUMN T_SYS_LOG.ip
IS 'ip地址';
COMMENT ON COLUMN T_SYS_LOG.create_time
IS '操作时间';

------------------------------------------

------------------------------------

-- Create table
CREATE TABLE T_SYS_MENU
(
  id          VARCHAR2(24) PRIMARY KEY,
  menu_name   VARCHAR2(20)              NOT NULL,
  authority   VARCHAR2(40),
  menu_url    VARCHAR2(40),
  parent_id   VARCHAR2(24) DEFAULT '-1' NOT NULL,
  is_menu     NUMBER(1) DEFAULT '0'     NOT NULL,
  sort        NUMBER(5) DEFAULT '0'     NOT NULL,
  menu_icon   VARCHAR2(20),
  create_date DATE DEFAULT sysdate,
  update_date DATE DEFAULT sysdate
)

-- Add comments to the table 
COMMENT ON TABLE T_SYS_MENU
IS '权限表';
-- Add comments to the columns 
COMMENT ON COLUMN T_SYS_MENU.id
IS '权限id';
COMMENT ON COLUMN T_SYS_MENU.menu_name
IS '菜单名称';
COMMENT ON COLUMN T_SYS_MENU.authority
IS '授权标识';
COMMENT ON COLUMN T_SYS_MENU.menu_url
IS '菜单url';
COMMENT ON COLUMN T_SYS_MENU.parent_id
IS '父id';
COMMENT ON COLUMN T_SYS_MENU.is_menu
IS '0菜单，1按钮';
COMMENT ON COLUMN T_SYS_MENU.sort
IS '排序号';
COMMENT ON COLUMN T_SYS_MENU.menu_icon
IS '菜单图标';
COMMENT ON COLUMN T_SYS_MENU.create_date
IS '创建时间';
COMMENT ON COLUMN T_SYS_MENU.update_date
IS '修改时间';
---------------------------------------------
INSERT INTO t_sys_menu
VALUES ('1', '系统管理', NULL, NULL, '-1', '0', '1', 'layui-icon-set', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('10', '修改角色', 'role:edit', '', '7', '1', '35', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('11', '删除角色', 'role:delete', '', '7', '1', '37', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('12', '角色权限管理', 'role:auth', '', '7', '1', '39', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('13', '菜单管理', NULL, 'system/menu', '1', '0', '50', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('14', '查询菜单', 'menu:view', '', '13', '1', '51', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('15', '添加菜单', 'menu:addBatch', '', '13', '1', '53', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('16', '修改菜单', 'menu:edit', '', '13', '1', '55', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('17', '删除菜单', 'menu:delete', '', '13', '1', '57', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('18', '字典管理', NULL, 'system/dict', '1', '0', '70', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('19', '字典查询', 'dict:view', '', '18', '1', '71', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('2', '用户管理', NULL, 'system/user', '1', '0', '10', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('20', '字典添加', 'dict:addBatch', NULL, '18', '1', '73', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('21', '字典修改', 'dict:edit', NULL, '18', '1', '75', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('22', '字典删除', 'dict:delete', NULL, '18', '1', '77', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('23', '系统监控', '', '', '-1', '0', '100', 'layui-icon-senior', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('24', '系统日志', 'log:view', 'monitor/log', '23', '0', '110', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('25', 'Druid监控', 'druid:view', 'monitor/druid', '23', '0', '130', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('26', '批量任务', NULL, NULL, '-1', '0', '200', 'layui-icon-find-fill', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('27', '基本任务', NULL, '/batch/task', '26', '0', '210', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('28', '任务计划', NULL, 'batch/schedule', '26', '0', '230', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('29', '查询任务', 'task:view', NULL, '27', '1', '211', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('3', '查询用户', 'user:view', '', '2', '1', '11', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('30', '新增任务', 'task:addBatch', NULL, '27', '1', '213', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('31', '修改任务', 'task:edit', NULL, '27', '1', '215', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('32', '删除任务', 'task:delete', NULL, '27', '1', '217', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('33', '查询计划', 'schedule:view', NULL, '28', '1', '231', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('34', '新增计划', 'schedule:addBatch', NULL, '28', '1', '233', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('35', '修改计划', 'schedule:edit', NULL, '28', '1', '235', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('36', '删除计划', 'schedule:delete', NULL, '28', '1', '237', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('37', '启动计划', 'schedule:run', NULL, '28', '1', '239', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('38', '暂停计划', 'schedule:pause', NULL, '28', '1', '241', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('39', '恢复计划', 'schedule:resume', NULL, '28', '1', '243', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('4', '添加用户', 'user:addBatch', NULL, '2', '1', '13', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('5', '修改用户', 'user:edit', NULL, '2', '1', '15', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('6', '删除用户', 'user:delete', NULL, '2', '1', '17', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('7', '角色管理', NULL, 'system/role', '1', '0', '30', NULL, to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('8', '查询角色', 'role:view', '', '7', '1', '31', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_menu
VALUES ('9', '添加角色', 'role:addBatch', '', '7', '1', '33', '', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));

----------------------------------------------
-- Create table
CREATE TABLE T_SYS_ROLE
(
  id          VARCHAR2(24) PRIMARY KEY,
  role_name   VARCHAR2(20) NOT NULL,
  comments    VARCHAR2(100),
  create_date DATE DEFAULT sysdate,
  update_date DATE DEFAULT sysdate
)
-- Add comments to the table 
COMMENT ON TABLE T_SYS_ROLE
IS '角色表';
-- Add comments to the columns 
COMMENT ON COLUMN T_SYS_ROLE.id
IS '角色id';
COMMENT ON COLUMN T_SYS_ROLE.role_name
IS '角色名称';
COMMENT ON COLUMN T_SYS_ROLE.comments
IS '备注';
COMMENT ON COLUMN T_SYS_ROLE.create_date
IS '创建时间';
COMMENT ON COLUMN T_SYS_ROLE.update_date
IS '修改时间';

-------------------------------------------
INSERT INTO t_sys_role
VALUES ('1', '超级管理员', '超级管理员', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role
VALUES ('2', '管理员', '系统管理员', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role
VALUES ('3', '普通用户', '系统普通用户', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'), to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));

--------------------------------------------
-- Create table
CREATE TABLE T_SYS_ROLE_MENU
(
  role_id     VARCHAR2(24) NOT NULL,
  menu_id     VARCHAR2(24) NOT NULL,
  create_date DATE DEFAULT sysdate
)

-- Add comments to the table 
COMMENT ON TABLE T_SYS_ROLE_MENU
IS '角色菜单(权限)关联表';
-- Add comments to the columns 
COMMENT ON COLUMN T_SYS_ROLE_MENU.role_id
IS '角色id';
COMMENT ON COLUMN T_SYS_ROLE_MENU.menu_id
IS '权限id';
-----------------------------------------
INSERT INTO t_sys_role_menu
VALUES ('1', '1', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '2', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '3', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '4', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '5', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '6', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '7', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '8', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '9', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '10', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '11', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '12', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '13', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '14', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '15', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '16', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '17', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '18', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '19', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '20', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '23', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '24', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '25', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '26', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '27', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '29', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '30', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '31', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '32', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '28', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '33', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '34', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '35', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '36', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '37', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '38', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('1', '39', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '26', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '28', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '33', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '34', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '35', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '36', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '37', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '38', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('3', '39', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '1', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '2', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '3', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '4', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '5', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '6', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '7', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '8', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '9', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '10', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '11', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '12', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '13', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '14', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '15', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '16', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '17', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '18', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '19', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '20', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '21', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '22', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '23', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '24', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '25', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '26', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '27', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '29', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '30', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '31', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '32', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '28', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '33', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '34', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '35', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '36', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '37', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '38', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_role_menu
VALUES ('2', '39', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
-----------------------------------------
-- Create table
CREATE TABLE T_SYS_USER
(
  id          VARCHAR2(24) PRIMARY KEY,
  user_name   VARCHAR2(20)            NOT NULL,
  password    VARCHAR2(128)           NOT NULL,
  real_name   VARCHAR2(20),
  nick_name   VARCHAR2(20)            NOT NULL,
  sex         VARCHAR2(2) DEFAULT '男' NOT NULL,
  phone       VARCHAR2(12),
  avatar      VARCHAR2(200),
  email       VARCHAR2(100),
  state       NUMBER(1) DEFAULT '0'   NOT NULL,
  create_date DATE DEFAULT sysdate,
  update_date DATE DEFAULT sysdate,
  CONSTRAINT USER_ACCOUNT UNIQUE (user_name)
)

COMMENT ON TABLE T_SYS_USER
IS '用户表';
-- Add comments to the columns 
COMMENT ON COLUMN T_SYS_USER.id
IS '用户id';
COMMENT ON COLUMN T_SYS_USER.user_name
IS '账号';
COMMENT ON COLUMN T_SYS_USER.password
IS '密码';
COMMENT ON COLUMN T_SYS_USER.real_name
IS '真实姓名';
COMMENT ON COLUMN T_SYS_USER.nick_name
IS '昵称';
COMMENT ON COLUMN T_SYS_USER.sex
IS '性别';
COMMENT ON COLUMN T_SYS_USER.phone
IS '手机号';
COMMENT ON COLUMN T_SYS_USER.avatar
IS '头像';
COMMENT ON COLUMN T_SYS_USER.email
IS '邮箱';
COMMENT ON COLUMN T_SYS_USER.state
IS '状态，0-正常，1-冻结';
COMMENT ON COLUMN T_SYS_USER.create_date
IS '注册时间';
COMMENT ON COLUMN T_SYS_USER.update_date
IS '修改时间';

------------------------------------------
INSERT INTO t_sys_user
VALUES ('1',
        'superAdmin',
        '$2a$04$zL8ODSgbRL.TQl9cIg5XUOiVevq4Ip2r/YJ1eXxODXnf9EaAQKvhu',
        NULL,
        '超级管理员',
        '男',
        '12345678901',
        NULL,
        NULL,
        '0',
        to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'),
        to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_user
VALUES ('2',
        'admin',
        '$2a$04$KY7D/USYlMptFiHDWO/K.OIfqnhRhI89iWiE.17zoyD1Ew8oGiism',
        NULL,
        '管理员',
        '男',
        '12345678901',
        NULL,
        NULL,
        '0',
        to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'),
        to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_user
VALUES ('3',
        'Sam',
        '$2a$04$CnnjFZciwvm9gjw9Au.ZeOJRaJOKbIE.Sf/4dQdYKp4U06fQL5tCa',
        NULL,
        'sam',
        '男',
        '13611111111',
        NULL,
        NULL,
        '0',
        to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'),
        to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));

------------------------------------------
-- Create table
CREATE TABLE T_SYS_USER_ROLE
(
  user_id     VARCHAR2(24) NOT NULL,
  role_id     VARCHAR2(24) NOT NULL,
  create_date DATE DEFAULT sysdate
)

-- Add comments to the table 
COMMENT ON TABLE T_SYS_USER_ROLE
IS '用户角色关联表';
-- Add comments to the columns 
COMMENT ON COLUMN T_SYS_USER_ROLE.user_id
IS '用户id';
COMMENT ON COLUMN T_SYS_USER_ROLE.role_id
IS '角色id';
-------------------------------------------
INSERT INTO t_sys_user_role
VALUES ('1', '1', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_user_role
VALUES ('2', '2', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO t_sys_user_role
VALUES ('3', '3', to_date('2018-09-15 12:09:09', 'yyyy-mm-dd hh24:mi:ss'));

